# README #

Displays all inputs/buttons of the first two connected joystick/joypads. Should work on any platform (Android, iOS, PC, consoles) and with any connected controller. Use to find out which button or analogue sticks axis maps to which input.

Note: The controller should be attached before starting the application.

### How do I get set up? ###

* Use the latest version of Unity 3D (https://unity3d.com/get-unity/download). Open a project in Unity and select this projects folder to open.
* Configuration: Use Build Settings to select your target device.
* Deployment instructions: Follow Unity docs for your target device.

### Who do I talk to? ###

* Repo owner or admin
* More info: https://www.gmtdev.com/unity-input-and-joystick-tester/
