/*/////////////////////////////////////////////////////////////////////////////////////////////////
InputListener.cs
Monitors, reads and displays all inputs, digital and analouge, on the first two joypads connected. 

  - Also supports Apple TV tvOS input devices like the Apple TV Remote. 
  - Should just work on any target (iOS, Android, consoles, PC, etc).

Example:

Related Components / Scripts:

Created (date - name): 
2015-??-??? - Gavin M Thornton

Last Updated (date - name - Purpose): 
2017-09-20 - Gavin M Thornton - UI update

/////////////////////////////////////////////////////////////////////////////////////////////////*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

#if UNITY_TVOS
using UnityEngine.Apple.TV;
#endif

public class InputsLister : MonoBehaviour
{
    public bool touchesEnabledSetting;

    Text[] axis = new Text[28];
    Text[] button = new Text[20];
    Text[] axisJ2 = new Text[28];
    Text[] buttonJ2 = new Text[20];
    public Text connectedLabel;
    public Text connectedValue;
    public Text infoLabel;
    public Text infoValue;
    public Text infoValue2;

    public Text connectedLabel_j2;
    public Text connectedValue_j2;

    void Awake()
    {
        infoLabel.text = "";
        infoValue.text = "";
        infoValue2.text = "";

        #if UNITY_TVOS
        infoLabel.text = "AppleTV tvOS Info";
        #endif

        for (int count = 0; count < 28; count++)
        {
            string name = "Panel/Joystick 1/Axis " + (count + 1) + " value";
            axis[count] = transform.Find(name).gameObject.GetComponent<Text>();
            Debug.Log("set");
        }

        for (int count = 0; count < 20; count++)
        {
            string name = "Panel/Joystick 1/Button " + count + " value";
            button[count] = transform.Find(name).gameObject.GetComponent<Text>();
        }

        for (int count = 0; count < 28; count++)
        {
            string name = "Panel/Joystick 2/Axis " + (count + 1) + " value";
            axisJ2[count] = transform.Find(name).gameObject.GetComponent<Text>();
            Debug.Log("set");
        }

        for (int count = 0; count < 20; count++)
        {
            string name = "Panel/Joystick 2/Button " + count + " value";
            buttonJ2[count] = transform.Find(name).gameObject.GetComponent<Text>();
        }

        #if UNITY_TVOS
        if (!Application.isEditor)
            UnityEngine.Apple.TV.Remote.touchesEnabled = touchesEnabledSetting;        
        #endif
    }

    // Update is called once per frame
    void Update()
    {
        #if UNITY_TVOS
        if (!Application.isEditor)      // Can only do the below on a real tvOS
        {
            // touchesEnabled == true should mean "Disables Apple TV Remote touch propagation to Unity Input.touches API"
            // http://docs.unity3d.com/ScriptReference/Apple.TV.Remote-touchesEnabled.html
            // ........ but it doesn't seem to be working.
            if( UnityEngine.Apple.TV.Remote.touchesEnabled )
                infoValue.text = "touchedEnabled=true (Unity's Inputs from remote should be disabled and you should be seeing no values)";
            else
                infoValue.text = "touchedEnabled=false (Unity's Inputs from remote shoudl be working)";

            // reportAbsoluteDpadValues == true gives you real location axis on the pad (0,0 is middle, 1.1 top right corner).
            // Jobs a good'un (it works).
            // http://docs.unity3d.com/ScriptReference/Apple.TV.Remote-reportAbsoluteDpadValues.html
            if( UnityEngine.Apple.TV.Remote.reportAbsoluteDpadValues )
                infoValue2.text = "reportAbsoluteDpadValues=true (direct touch)";
            else
                infoValue2.text = "reportAbsoluteDpadValues=false (swipe)";
        }
        #endif

        var controllers = Input.GetJoystickNames();
        if (controllers.Length > 0)
        {
            // Joystick has just been connected
            connectedLabel.text = "Joystick 1 Connected";
            connectedValue.text = controllers[0];
        }
        else
        {
            connectedLabel.text = "No Joysticks Connected";
            connectedValue.text = "If in editor try restarting Unity with Joysick connected";
        }

        if (controllers.Length > 1)
        {
            // Joystick has just been connected
            connectedLabel_j2.text = "Joystick 2 Connected";
            connectedValue_j2.text = controllers[1];
        }
        else
        {
            connectedLabel_j2.text = "No Joysticks Connected";
            connectedValue_j2.text = "...beep beep";
        }

        string input;
        for (int count = 0; count < 28; count++)
        {
            input = "Axis " + (count + 1);

            axis[count].text = "" + Input.GetAxis(input).ToString("F2");
        }

        for (int count = 0; count < 28; count++)
        {
            input = "J2 Axis " + (count + 1);

            axisJ2[count].text = "" + Input.GetAxis(input).ToString("F2");
        }

        string up = "up";
        string down = "down";
        int buttonCount = 0;

        #region Button Input
        if (Input.GetKey(KeyCode.Joystick1Button0) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button1) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button2) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button3) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button4) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button5) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button6) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button7) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button8) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button9) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button10) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button11) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button12) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button13) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button14) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button15) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button16) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button17) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button18) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick1Button19) == true)
            button[buttonCount++].text = down;
        else
            button[buttonCount++].text = up;

        //buttons
        buttonCount = 0;
        if (Input.GetKey(KeyCode.Joystick2Button0) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button1) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button2) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button3) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button4) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button5) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button6) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button7) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button8) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button9) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button10) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button11) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button12) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button13) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button14) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button15) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button16) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button17) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button18) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;

        if (Input.GetKey(KeyCode.Joystick2Button19) == true)
            buttonJ2[buttonCount++].text = down;
        else
            buttonJ2[buttonCount++].text = up;
        #endregion // Button Input
    }
}
